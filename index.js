const express = require('express');
const axios = require('axios');

const app = express();

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});


app.get('/users', async (req, res) => {
    const resp = await axios.get('https://jsonplaceholder.typicode.com/users')
        .then(res => {
            return res.data;
        })
        .catch(error => {
            res.status(500).json({type: 'error', message: error.message});
        });
    res.status(200);
    res.send(resp);
    res.end;

});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`listening on ${PORT}`));
